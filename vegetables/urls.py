from __future__ import absolute_import, unicode_literals
from django.conf.urls import patterns, url
from mezzanine.conf import settings
from . import views

_slash = "/" if settings.APPEND_SLASH else ""

urlpatterns = patterns("cartridge.shop.views",
    url("^(?P<slug>.*)%s$" % _slash, "product", name="shop_product"),
)
