"""
    filter to get the annoying variations name.ie 'Product Size: Variatio_name'
    is dislike it and its ruining my frontend presentation
    TODO: fix that rant
"""
from django import template
register = template.Library()

@register.filter
def shave(string):
    """
    Display only the actual variation name
    """
    if ':' in str(string):
        variation = str(string).split(':', 2)
        return variation[1].strip()
    else:
        return ' Each'
