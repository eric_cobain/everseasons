# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import cartridge.shop.fields


class Migration(migrations.Migration):

    dependencies = [
        ('sites', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='CustomOrder',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('billing_detail_first_name', models.CharField(max_length=100, verbose_name='First name')),
                ('billing_detail_last_name', models.CharField(max_length=100, verbose_name='Last name')),
                ('billing_detail_county_estate', models.CharField(max_length=100, verbose_name='County/Suburb')),
                ('billing_detail_house_address', models.TextField(verbose_name='Estate name/                                                Street address/                                                House address(Number)/                                                Address description')),
                ('billing_detail_phone', models.CharField(max_length=20, verbose_name='Phone')),
                ('billing_detail_email', models.EmailField(max_length=254, verbose_name='Email')),
                ('shipping_detail_first_name', models.CharField(max_length=100, verbose_name='First name')),
                ('shipping_detail_last_name', models.CharField(max_length=100, verbose_name='Last name')),
                ('shipping_detail_county_estate', models.CharField(max_length=100, verbose_name='County/Suburb')),
                ('shipping_detail_house_address', models.TextField(verbose_name='Estate name/                                                       Street address/                                                       House address(Number)/                                                       Address description')),
                ('shipping_detail_phone', models.CharField(max_length=20, verbose_name='Phone')),
                ('additional_instructions', models.TextField(blank=True, verbose_name='Additional instructions')),
                ('time', models.DateTimeField(null=True, auto_now_add=True, verbose_name='Time')),
                ('key', models.CharField(max_length=40, db_index=True)),
                ('user_id', models.IntegerField(blank=True, null=True)),
                ('shipping_type', models.CharField(blank=True, max_length=50, verbose_name='Shipping type')),
                ('shipping_total', cartridge.shop.fields.MoneyField(blank=True, null=True, decimal_places=2, max_digits=10, verbose_name='Shipping total')),
                ('tax_type', models.CharField(blank=True, max_length=50, verbose_name='Tax type')),
                ('tax_total', cartridge.shop.fields.MoneyField(blank=True, null=True, decimal_places=2, max_digits=10, verbose_name='Tax total')),
                ('item_total', cartridge.shop.fields.MoneyField(blank=True, null=True, decimal_places=2, max_digits=10, verbose_name='Item total')),
                ('discount_code', cartridge.shop.fields.DiscountCodeField(blank=True, max_length=20, verbose_name='Discount code')),
                ('discount_total', cartridge.shop.fields.MoneyField(blank=True, null=True, decimal_places=2, max_digits=10, verbose_name='Discount total')),
                ('total', cartridge.shop.fields.MoneyField(blank=True, null=True, decimal_places=2, max_digits=10, verbose_name='Order total')),
                ('transaction_id', models.CharField(blank=True, null=True, max_length=255, verbose_name='Transaction ID')),
                ('status', models.IntegerField(default=1, verbose_name='Status', choices=[(1, 'Unprocessed'), (2, 'Processed')])),
                ('site', models.ForeignKey(to='sites.Site', editable=False)),
            ],
            options={
                'verbose_name_plural': 'Orders',
                'ordering': ('-id',),
                'verbose_name': 'Order',
            },
        ),
    ]
