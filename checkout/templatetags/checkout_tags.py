from django import template
from django.forms import HiddenInput

register = template.Library()

@register.filter
def is_input_filled(value):
    """
    Check if the form field is hidden.
    The logic used here if even very important fields are set to required
    just checking if they are filled is enough.
    """
    if not value:
        return False
    else:
        return True

@register.filter
def is_hidden(field):
    """
    Check if the field is hidden.
    """
    if not field.is_hidden:
        return False
    else:
        return True
