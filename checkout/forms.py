from django import forms
from django.utils.translation import ugettext_lazy as _

from mezzanine.conf import settings

from cartridge.shop.forms import OrderForm
from cartridge.shop import checkout
from cartridge.shop.utils import make_choices

from .counties import get_county_names_list, get_estate_names_list

class EverseasonsOrderForm(OrderForm):
    """
        Custom Order Form, billing shipping and payment form.
        using custom addressing forms so theres need to hide the
        ones that comee with cartridge.
        Plus additional payment fields for use with mobile money.
    """

    def __init__(self, request, step, data=None, initial=None, errors=None):
        OrderForm.__init__(self, request, step, data, initial, errors)

        is_first_step = step == checkout.CHECKOUT_STEP_FIRST
        is_last_step = step == checkout.CHECKOUT_STEP_LAST
        is_payment_step = step == checkout.CHECKOUT_STEP_PAYMENT

        unwanted = ['state', 'street', 'postcode', 'country', 'city']

        # get list of county names
        counties = make_choices(get_county_names_list())
        estates = make_choices(get_estate_names_list("Nairobi"))

        if settings.SHOP_CHECKOUT_STEPS_SPLIT:
            if is_first_step:
                #self.fields['billing_detail_county'].widget = forms.Select(choices=counties)
                #self.fields['billing_detail_county'].initial = settings.SHOP_DEFAULT_COUNTY
                #self.fields['billing_detail_estate'].widget = forms.Select(choices=estates)
                #self.fields['shipping_detail_county'].widget = forms.Select(choices=counties)
                #self.fields['shipping_detail_county'].initial = settings.SHOP_DEFAULT_COUNTY
                #self.fields['shipping_detail_estate'].widget = forms.Select(choices=estates)
                # Change country widgets to a Select widget
                for field in self.fields:
                    for name in unwanted:
                        if field.endswith(name) and not field.endswith('estate')\
                                or field.startswith('mpesa_'):
                            self.fields[field].widget = forms.HiddenInput()
                            self.fields[field].required = False
            if is_payment_step:
                for field in self.fields:
                    if field.startswith('card_'):
                        self.fields[field].widget = forms.HiddenInput()
                        self.fields[field].required = False


class AddToCartForm():
    pass
