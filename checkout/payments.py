"""
    payment handling
"""
from __future__ import unicode_literals
from mezzanine.conf import settings
from mezzanine.accounts import get_profile_for_user, ProfileNotConfigured
from cartridge.shop.models import Order
from cartridge.shop.utils import set_shipping, set_tax, sign

class CheckoutError(Exception):

    pass

def payment_handler(request, order_form, order):
    """
    Handle payments when order is complete. 
    """
    pass
