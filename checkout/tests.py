from django.test import TestCase
from checkout.forms import EverseasonsOrderForm

class EverseasonsOrderFormTest(TestCase):

    def test_all_form_fields_are_present(self):
        fields = EverseasonOrderForm._meta.fields
        expected_fields = ['billing_detail_first_name',
        'billing_detail_last_name', 'billing_detail_street',
        'billing_detail_city','billing_detail_state',
        'billing_detail_postcode', 'billing_detail_country',
        'billing_detail_phone', 'billing_detail_email',
        'shipping_detail_first_name', 'shipping_detail_last_name',
        'shipping_detail_street', 'shipping_detail_city',
        'shipping_detail_state', 'shipping_detail_postcode',
        'shipping_detail_country', 'shipping_detail_phone',
        'additional_instructions', 'discount_code']

        self.assertEqual(expected_fields, fields)
