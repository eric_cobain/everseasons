"""
Shipping handler module
"""

from __future__ import unicode_literals

from django.utils.translation import ugettext_lazy as _
from mezzanine.conf import settings
from cartridge.shop.utils import set_shipping


def shipping_handler(request, order_form):
    """
    Charge shipping defined in 'SHOP_DEFUALT_SHIPPING_VALUE' in the admin
    for purchases less than amount defined in 'SHOP_FREE_DELIVERY_MINIMUM_ORDER_TOTAL'.
    changes to this rates maybe frequent and unpredictable so having it in remain in
    the admin is ideal.
    """

    if not request.session.get("free_shipping"):
        settings.use_editable()
        if request.cart.total_price() < settings.SHOP_FREE_DELIVERY_MINIMUM_ORDER_TOTAL:
            shipping_total = settings.SHOP_DEFAULT_SHIPPING_VALUE
        else:
            shipping_total = 0
        set_shipping(request, _("Flat delivery fee"), shipping_total)
