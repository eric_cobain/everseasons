from copy import deepcopy

from django.contrib import admin
from cartridge.shop.admin import OrderAdmin
from cartridge.shop.models import Order

order_fieldsets = deepcopy(OrderAdmin.fieldsets)

billing = order_fieldsets[0][1]["fields"]
billing.remove(('billing_detail_postcode', 'billing_detail_country'))
billing.remove(('billing_detail_city', 'billing_detail_state'))
billing.remove(('billing_detail_last_name', 'billing_detail_street'))
billing.insert(3, "billing_detail_last_name")
billing.insert(4, "paid")

shipping = order_fieldsets[1][1]["fields"]
shipping.remove(('shipping_detail_postcode', 'shipping_detail_country'))
shipping.remove(('shipping_detail_city', 'shipping_detail_state'))
shipping.remove(('shipping_detail_last_name', 'shipping_detail_street'))
shipping.insert(2, "shipping_detail_last_name")

other = order_fieldsets[2][1]

class EverseasonsOrderAdmin(OrderAdmin):
    fieldsets = order_fieldsets

admin.site.unregister(Order)
admin.site.register(Order, EverseasonsOrderAdmin)
