"""
List if the 47 Counties in Kenya
"""

def get_county_names_list():
    return COUNTIES.keys()

def get_estate_names_list(county):
    return COUNTIES[county]

COUNTIES = {
    "Nairobi": ["South B", "South C"],
}
