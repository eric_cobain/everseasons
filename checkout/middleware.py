"""
Custom middleware.. first attempt
"""
from django.core import serializers
from django.shortcuts import get_object_or_404
from django.contrib.messages import info
from django.http import JsonResponse

from cartridge.shop.forms import AddProductForm
from cartridge.shop.models import Product, ProductVariation
from cartridge.shop.utils import recalculate_cart

# sorl not a dependacy
#from sorl.thumbnail import get_thumbnail

class AjaxAddToCartMiddleware(object):
    """
    Add to cart when add_to_cart form is submitted from any page
    TODO: Error Handling, Testing and using a ProperForm
    """

    def add_to_cart(self, request):
        form_class = AddProductForm
        published_products = Product.objects.published(
                                for_user=request.user)
        product = get_object_or_404(published_products,
                    id=request.POST.get("product"))
        variation = product.variations.first()
        to_cart = (request.method == 'POST' and
                   request.POST.get("add_wishlist") is None)
        add_product_form = form_class(request.POST, product=product,
                                      to_cart=to_cart)
        if to_cart:
            quantity = int(request.POST.get('quantity'))
            request.cart.add_item(variation, quantity)
            recalculate_cart(request)
            data =  { "msg": "Added \"" + product.title.capitalize() +
                      "\" to Shopping Cart"}
            return data

    def process_request(self, request):
        if request.is_ajax() and request.method == 'POST'\
                                    and 'add_to_cart' in request.POST:
            data = {"product": request.POST.get('product'),
                    "quantity": request.POST.get('quantity'),
                   }
            response = self.add_to_cart(request)
            print [get_thumbnail(item.image, '40x40', crop="center", quality=99) for item in cart.items]
            cart = serializers.serialize('json', request.cart)
            response['cart'] = cart
