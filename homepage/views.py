from django.shortcuts import redirect
from cartridge.shop.models import Product
from django.views import generic

class HomePage(generic.TemplateView):

    template_name = 'index.html'

    def post(self, request, *args, **kwargs):
        return redirect('home')

    def get_context_data(self, **kwargs):
        context = super(HomePage, self).get_context_data(**kwargs)
        products = Product.objects.all()[:10]
        featured = products[:3]
        context['products'] = products
        context['featured'] = featured
        return context
